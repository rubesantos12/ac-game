package org.academiadecodigo.cunnilinux;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Menu {



    private Picture menu = new Picture(0, 0, Images.MENU_BRIGHT);
    private boolean onMenu = true;
    private MenuKeyboard menuKeyboard = new MenuKeyboard(new MenuLogic(this));

    public void startMenu() throws InterruptedException {
        menu.draw();
        while (onMenu) {
            menu.load(Images.MENU_BRIGHT);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            menu.load(Images.MENU_SHADOW);

        }
        menu.delete();
    }

    public void setOnMenu(boolean onMenu) {
        this.onMenu = onMenu;
    }

    private class MenuKeyboard {

        public MenuKeyboard(KeyboardHandler menuLogic) {

            Keyboard keyboard = new Keyboard(menuLogic);

            KeyboardEvent start = new KeyboardEvent();
            KeyboardEvent leave = new KeyboardEvent();

            start.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
            leave.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

            start.setKey(KeyboardEvent.KEY_Q);
            leave.setKey(KeyboardEvent.KEY_L);

            keyboard.addEventListener(start);
            keyboard.addEventListener(leave);
        }

    }

    private class MenuLogic implements KeyboardHandler {

        private Menu menu;

        public MenuLogic(Menu menu) {
            this.menu = menu;
        }

        @Override
        public void keyPressed(KeyboardEvent keyboardEvent) {
            if (keyboardEvent.getKey() == KeyboardEvent.KEY_Q) {
                menu.setOnMenu(false);
            }
            if (keyboardEvent.getKey() == KeyboardEvent.KEY_L) {
                System.exit(0);
            }
        }

        @Override
        public void keyReleased(KeyboardEvent keyboardEvent) {

        }


    }

}
