package org.academiadecodigo.cunnilinux.player;


import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Commander extends Player{

    private int health;
    private int xp;
    private int levelNumber ;


    //Commander constructor
    public Commander(int health, int xp, int levelNumber) {
        super(health, xp, levelNumber);
    }



    //Methods from Keyboard Handler
    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}
