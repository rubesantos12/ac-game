package org.academiadecodigo.cunnilinux.player;

import org.academiadecodigo.cunnilinux.Images;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.awt.image.BufferedImage;

public abstract class Player implements KeyboardHandler {
    private int health;
    private int xp = 0;
    private int levelNumber;
    private Text level;
    private String levelString;
    private Text hp;
    private String healthString;
    private boolean jumped = false;
    private boolean dead;
    public BufferedImage swing1, swing2, swing3;

    //Create XpBar, HPBar, weapon and define player picture
    Rectangle xpBar = new Rectangle(75, 15, 100, 20);
    Rectangle hpBar = new Rectangle(75,50, 100,20);
    Picture player = new Picture(120, 735, "ac-game/sprites/Game_Images/knight_f_idle_anim_f0.png");

    //Player constructor
    public Player(int health, int xp, int levelNumber) {
        this.health = health;
        this.xp = xp;
        this.levelNumber = levelNumber;
        player.grow(13,11);
    }

    public void growXpBar () {
        //Check the player XP, if it's higher than 500, he levels up
        if (xp < 100) {
            xpBar.grow(4, 0);
            xp += 5;
        } else {
            //If he levels up, get the xp bar back to normal size
            xpBar.grow(-80, 0);
            //Define xp back to 0
            xp = 0;
            //Remove the old level string
            level.delete();
            //Update the level number
            levelNumber = levelNumber + 1;
            //Redraw the level string with the new updated number
            level.setText("Level " + levelNumber);
            level.draw();
            //Increase the health
            health += 5;
            //Update the health string with the new updated number
            hp.setText("Health " + health);
            //Remove old string and draw the new one
            hp.delete();
            hp.draw();
        }
    }

    public void updateHpBar() {
       hp.setText("Health " + health);
    }


    public void gravity() {
        if(player.getY() < 735) {
            player.translate(0, 20);
        }
    }

    //Draw the player
    public void drawPlayer() {

        player.draw();
    }
    public void showBothBars() {
        //Set the level bar string
        levelString = "Level 1";
        level = new Text(80, 15, levelString);
        //Set the health bar string
        healthString = "Health " + health;
        hp = new Text(80, 50, healthString);
        //Set bar background color
        xpBar.setColor(Color.WHITE);
        hpBar.setColor(Color.WHITE);
        //Fill the bars
        xpBar.fill();
        hpBar.fill();
        //Draw the text
        level.draw();
        hp.draw();
    }

    //player movement
    public void move (int x, int y) {
        player.translate(x,y);
    }

    //Check the X and Y position of the player, so he doesn't leave the screen
    public void update() {
        if (player.getY() >=  795) {
            player.translate(0,-20);
        }
        else if (player.getX() - 100 <= 0) { //if speed would put character outside of screen
            player.translate(50,0); //sets his X to limit
        } else if (player.getX() + 40 >= 1780) {
            player.translate(-50,0);
        }
    }

    public void idle () {
        if (player.getY() > 735) {
            player.load("ac-game/sprites/Game_Images/knight_f_idle_anim_f0.png");
            setJumped(false);
        }
    }

    public void jumpSprite() {
        player.load("ac-game/sprites/Game_Images/knight_m_hit_anim_f0.png");
    }

    public void attack() {
        player.load(Images.SWING_1);
            player.load(Images.SWING_2);
            player.load(Images.SWING_3);
    }

    public void load (String image) {
        player.load(image);
    }

    public Rectangle playerBounds() {
        return new Rectangle(120, 735, 13, 11);
    }

    //Getters and setters
    public int getYCord() {
        return player.getY();
    }

    public int getXCord() {
        return player.getX();
    }

    public boolean isJumped() {
        return jumped;
    }

    public void setJumped(boolean jumped) {
        this.jumped = jumped;
    }

    public void setHealth(int damage) {
        this.health = health - damage;
    }

    public int getHealth() {
        return health;
    }

    public boolean isDead() {
        return dead;
    }

    public Picture getPlayerPicture() {
        return player;
    }

    public void deletePlayer() {
        player.delete();
    }



}