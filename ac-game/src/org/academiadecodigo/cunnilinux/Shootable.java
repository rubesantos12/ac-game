package org.academiadecodigo.cunnilinux;

public interface Shootable {

    //Interface for everything that is shootable
    boolean isDestroyed();

    void hit(int damage);

    // methods are abstract
    public interface Destroyable {

        public abstract void hit(int damage);

        public abstract boolean isDestroyed();
    }
}
