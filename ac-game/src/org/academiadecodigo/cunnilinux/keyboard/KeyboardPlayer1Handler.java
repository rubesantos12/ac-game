package org.academiadecodigo.cunnilinux.keyboard;

import org.academiadecodigo.cunnilinux.enemy.Enemy;
import org.academiadecodigo.cunnilinux.enemy.EnemyPosition;
import org.academiadecodigo.cunnilinux.player.Commander;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class KeyboardPlayer1Handler extends KeyboardEvent implements KeyboardHandler{

    private Commander player;
    private Picture background1;
    private Enemy enemy;
    private int enemyX;
    private EnemyPosition enemyPosition;

    private boolean right;
    private boolean left;


    //keyboard Handler for player 1 constructor
    public KeyboardPlayer1Handler(Commander player, Picture background1, Enemy enemy) {
        this.player = player;
        this.background1 = background1;
        this.enemyPosition = enemy.getEnemyPosition();
    }


    //Make keyboard events on press
    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        enemy = enemyPosition.returnEnemy();
        enemyX = enemyPosition.returnEnemyX();

        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_LEFT: {
                if (player.getXCord() < 200 && background1.getX() < -300) {
                    background1.translate(10,0);
                }else player.move(-10, 0);
                break;
            }

            case KeyboardEvent.KEY_RIGHT: {
                if (player.getXCord() > 1500 && background1.getX() > -4450) {
                    background1.translate(-10,0);
                } else player.move(10, 0);
                break;
            }

            case KeyboardEvent.KEY_SPACE: {
                if (!player.isJumped()) {
                    player.jumpSprite();
                    player.setJumped(true);
                    player.move(0, -180);
                }
                break;
            }

           case KeyboardEvent.KEY_Z: {
               player.attack();
               if ((enemyX - player.getXCord() > 0) && (enemyX - player.getXCord() < 40)) {
                   enemy = enemy.getEnemyPosition().returnEnemy();
                   enemy.setDead(true);
                   break;
               }
               else if ((enemyX - player.getXCord() < 0) && (enemyX - player.getXCord() >-40)) {
                   enemy = enemy.getEnemyPosition().returnEnemy();
                   enemy.setDead(true);
                   break;
               }
            }
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_UP: {

            }
            case KeyboardEvent.KEY_DOWN: {

            }
            case KeyboardEvent.KEY_LEFT: {

            }
            case KeyboardEvent.KEY_RIGHT: {

            }
            case KeyboardEvent.KEY_Z: {

            }
        }
    }



}
