package org.academiadecodigo.cunnilinux.keyboard;

import org.academiadecodigo.cunnilinux.enemy.Enemy;
import org.academiadecodigo.cunnilinux.player.Commander;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class KeyboardPlayer1 extends KeyboardEvent{

    private Commander player1;

    private Picture background1;
    private Enemy enemy;

    public KeyboardPlayer1(Commander player1, Picture background1, Enemy enemy) {
        this.player1 = player1;
        this.background1 = background1;
        this.enemy = enemy;
    }

    public void keyboard() {
        KeyboardPlayer1Handler keyboardPlayer1Handler = new KeyboardPlayer1Handler(player1,background1,enemy);
        Keyboard keyboard = new Keyboard(keyboardPlayer1Handler);

        //Create keyboard event for all the keys
        KeyboardEvent rightArrow = new KeyboardEvent();
        KeyboardEvent leftArrow = new KeyboardEvent();
        KeyboardEvent shoot = new KeyboardEvent();
        KeyboardEvent jump = new KeyboardEvent();
        KeyboardEvent pause = new KeyboardEvent();

        //Create event listener for every key
        rightArrow.setKey(KeyboardEvent.KEY_RIGHT);
        rightArrow.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(rightArrow);

        leftArrow.setKey(KeyboardEvent.KEY_LEFT);
        leftArrow.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(leftArrow);

        shoot.setKey(KeyboardEvent.KEY_Z);
        shoot.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(shoot);

        jump.setKey(KeyboardEvent.KEY_SPACE);
        jump.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(jump);

        pause.setKey(KeyboardEvent.KEY_P);
        pause.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(pause);

    }
}
