package org.academiadecodigo.cunnilinux;

import org.academiadecodigo.cunnilinux.enemy.Enemy;
import org.academiadecodigo.cunnilinux.enemy.EnemyFactory;
import org.academiadecodigo.cunnilinux.keyboard.KeyboardPlayer1;
import org.academiadecodigo.cunnilinux.player.Commander;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Game {

    private static int delay = 150;
    private int levelCounter= 0;
    private boolean gameOver;
    private Picture background;
    private GameMusic music = new GameMusic();


    //Create background, start screen and player;
    Picture hud = new Picture(0,0, "ac-game/sprites/Game_Images/hud.png");
    Commander player1 = new Commander(20, 0, 1);
    Enemy enemy;
    KeyboardPlayer1 keyboardPlayer1;

    //Game constructor
    public Game(int delay) {
        this.delay = delay;
    }

    public static void main(String[] args) throws InterruptedException {
        Game game = new Game(125);
        game.init();
        game.start();
    }

    //Initiate the program
    public void init() {

    }

    //Start game
    public void start() throws InterruptedException {
        while (true) {
            setupLevel();

            while (levelCounter > 0 || gameOver) {
                if(player1.getHealth() <= 0) {
                    endGame();
                    break;
                }

                if (enemy.getEnemyPosition().getEnemyCounter() == 20 && levelCounter == 1) {  //endgame condition
                    levelCounter++;
                    setupLevel();
                }

                if (enemy.getEnemyPosition().getEnemyCounter() == 40 && levelCounter == 2) {
                    stageCleared();
                    credits();
                }



                //we start all the enemy spawn here or behaviors we want the player to have
                player1.update();
                player1.idle();
                player1.gravity();
                player1.updateHpBar();
                enemy.setNewPosition();

                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            levelCounter++;
            start();
        }
    }

    private void setupLevel() throws InterruptedException {
        switch (levelCounter) {
            case 0:
                setupMenu();
                break;
            case 1:
                setupStage();
                renderStage();
                break;
            case 2:
                setupStage();
                background.load(Images.BACKGROUND2);
                renderStage();
                hud.delete();
                player1.deletePlayer();
                player1.drawPlayer();
                hud.draw();
                player1.showBothBars();
                break;
            case 3:
                stageCleared();
                credits();
        }
    }

    private void renderStage() {
        background.draw();
        player1.drawPlayer();
        hud.draw();
        player1.showBothBars();
        enemy = EnemyFactory.createNewEnemy();
        enemy.getEnemyPosition().setPlayer(player1);
        keyboardPlayer1 = new KeyboardPlayer1(player1,background,enemy);
        keyboardPlayer1.keyboard();
    }

    private void setupStage () {
        background = new Picture(0,0,Images.BACKGROUND1); //missing sizes && background path
    }
    private void setupMenu() throws InterruptedException {
        Menu menu = new Menu();
        music.playMusic(Images.MENU_MUSIC);
        menu.startMenu();
        music.stopMusic();
        music.playMusic(Images.STAGE_MUSIC);
    }

    private void endGame() throws InterruptedException {
        if (levelCounter == 1) {
            gameOverFlick1();
        }
        else if (levelCounter == 2) {
            gameOverFlick2();
        }
        resetGameOverState();
    }

    private void resetGameOverState() {
        gameOver = false;
        levelCounter = -1;
        background.delete();
    }

    private void gameOverFlick1() {
        gameOver = true;
        player1.getPlayerPicture().delete();
        enemy.getPicture().delete();
        background.delete();
        background.draw();
        for (int i = 0; i < 50; i++) {
            background.load(Images.GAME_STAGE1_OVER_BRIGHT);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            background.load(Images.GAME_STAGE1_OVER_SHADOW);
        }
        System.exit(0);
        background.delete();
    }

    private void gameOverFlick2() {
        gameOver = true;
        player1.getPlayerPicture().delete();
        enemy.getPicture().delete();
        background.delete();
        background.draw();
        for (int i = 0; i < 50; i++) {
            background.load(Images.GAME_STAGE2_OVER_BRIGHT);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            background.load(Images.GAME_STAGE2_OVER_SHADOW);
        }
        System.exit(0);
        background.delete();
    }


    private void credits() {
        int i = 0;
        for (i = 0; i < 99999; i++) {
            background.load(Images.CREDITS_BRIGHT);

            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            background.load(Images.CREDITS_SHADOW);
        }
    }

    private void stageCleared() {
        music.stopMusic();
        music.playMusic(Images.CREDITS_MUSIC);
        player1.getPlayerPicture().delete();
        background.delete();
        background.draw();
        background.load(Images.CREDITS_BRIGHT);
    }

    private void winGame() {
        credits();
        System.exit(0);
    }
}