package org.academiadecodigo.cunnilinux;

public class Images {

    public final static String START_SCREEN = "ac-game/sprites/StartScreen/b0e51bb592bf036b0bcb8b7f7fbd2dca.png";

    public final static String MENU_BRIGHT = "ac-game/sprites/Game_Images/menu_Bright.png";
    public final static String MENU_SHADOW = "ac-game/sprites/Game_Images/menu_Shadow.png";
    public final static String BACKGROUND2 = "ac-game/sprites/Game_Images/background2.png";
    public final static String BACKGROUND1 = "ac-game/sprites/Game_Images/background1.png";
    public final static String GAME_STAGE1_OVER_BRIGHT = "ac-game/sprites/Game_Images/stage_1_over_Bright.png";
    public final static String GAME_STAGE1_OVER_SHADOW = "ac-game/sprites/Game_Images/stage_1_over_Shadow.png";
    public final static String GAME_STAGE2_OVER_BRIGHT = "ac-game/sprites/Game_Images/game_over_bright.png";
    public final static String GAME_STAGE2_OVER_SHADOW = "ac-game/sprites/Game_Images/game_over_shadow.png";

    public final static String CREDITS_BRIGHT = "ac-game/sprites/Game_Images/credits_bright.png";
    public final static String CREDITS_SHADOW = "ac-game/sprites/Game_Images/credits_shadow.png";

    public final static String SWING_1 = "ac-game/sprites/Game_Images/SWING_1.png";
    public final static String SWING_2 = "ac-game/sprites/Game_Images/SWING_2.png";
    public final static String SWING_3 = "ac-game/sprites/Game_Images/SWING_3.png";

    public final static String ENEMY_ORC_RIGHT = "";
    public final static String ENEMY_ORC_LEFT = "";
    public final static String Enemy_ORC_DEAD = "";

    public final static String MENU_MUSIC = "ac-game/sprites/Music/Menu_music.wav";
    public final static String STAGE_MUSIC = "ac-game/sprites/Music/Stage_music.wav";
    public final static String CREDITS_MUSIC = "ac-game/sprites/Music/Credits_music.wav";

}
