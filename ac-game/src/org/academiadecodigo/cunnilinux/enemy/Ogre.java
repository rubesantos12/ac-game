package org.academiadecodigo.cunnilinux.enemy;

public class Ogre extends Enemy{

    public Ogre(int health, int dmgInflicted) {
        super(health, dmgInflicted, EnemyType.OGRE, EnemyType.OGRE.getImagePath());
        getPicture().grow(13,11);
        getPicture().draw();
    }

}
