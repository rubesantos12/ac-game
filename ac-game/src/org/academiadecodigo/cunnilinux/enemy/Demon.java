package org.academiadecodigo.cunnilinux.enemy;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Demon extends Enemy{

    public Demon(int health, int dmgInflicted) {
        super(health, dmgInflicted, EnemyType.DEMON, EnemyType.DEMON.getImagePath());
        getPicture().grow(13,11);
        getPicture().draw();
    }

}
