package org.academiadecodigo.cunnilinux.enemy;

public class EnemyFactory {

    public static Enemy createNewEnemy() {
        int random = (int) (Math.random() * EnemyType.values().length);
        EnemyType enemyType = EnemyType.values()[random];
        Enemy enemy;

        switch (enemyType) {
            case KNIGHT:
                enemy = new Knight(3, 1);
                break;
            case DEMON:
                enemy = new Demon(5, 3);
                break;
            case GORILLA:
                enemy = new Gorilla(10,10);
                break;
            default:
                enemy = new Ogre(10, 5);
        }
        return enemy;
    }
}
