package org.academiadecodigo.cunnilinux.enemy;

public  enum EnemyType {
    KNIGHT(0,0, "ac-game/sprites/Game_Images/orc_warrior_idle_anim_f0.png"),
    DEMON(0,0, "ac-game/sprites/Game_Images/big_demon_idle_anim_f0.png"),
    OGRE(0, 0 , "ac-game/sprites/Game_Images/ogre_idle_anim_f0.png"),
    GORILLA(0,0,"ac-game/sprites/Game_Images/gorilla.png");

    private int health;
    private int dmgInflicted;
    private String imagePath;
    EnemyType (int health, int dmgInflicted, String imagePath) {
        this.health = health;
        this.dmgInflicted = dmgInflicted;
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return this.imagePath;
    }
}
