package org.academiadecodigo.cunnilinux.enemy;

public class Gorilla extends Enemy{

    public Gorilla(int health, int dmgInflicted) {
        super(health, dmgInflicted, EnemyType.GORILLA, EnemyType.GORILLA.getImagePath());
        getPicture().draw();
        getPicture().grow(13,11);
    }
}
