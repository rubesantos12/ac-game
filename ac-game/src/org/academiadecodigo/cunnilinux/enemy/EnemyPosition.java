package org.academiadecodigo.cunnilinux.enemy;

import org.academiadecodigo.cunnilinux.player.Player;

public class EnemyPosition {
    private int position;
    private double x = 1600;
    private double y = 735;
    private Player player;
    private Enemy enemy;
    private int enemyCounter;

    public EnemyPosition(Enemy enemy) {
        this.enemy = enemy;

    }

    public void enemyNewPosition() {

         if (player.getXCord() < enemy.getPicture().getX()){
             this.position = 0;
         } else {
             this.position = 1;
         }

        int DISTANCE = 10;
        setEnemy(enemy);
        System.out.println(enemy.getPicture().getY());
        System.out.println(player.getYCord());
        switch (this.position) {
            case 1:
                if (enemy.getPicture().getX() == player.getXCord() && enemy.getPicture().getY() + 5 == player.getYCord() && !enemy.isDestroyed()) {
                    player.setHealth(enemy.getDmgInflicted());
                    enemy.getPicture().delete();
                    enemy.setDead(true);
                    enemy.getPicture().translate(-1000,50);
                    break;
                } else if (!enemy.isDestroyed()){
                    setEnemy(enemy);
                    enemy.getPicture().translate(DISTANCE, 0.0);
                    break;
                } else if (enemy.isDestroyed()) {
                    enemy.setDead(false);
                    enemy.getPicture().delete();
                    enemy.getPicture().translate(-1000,50);
                    player.growXpBar();
                    enemy = createNewEnemy();
                    setEnemy(enemy);
                    enemyCounter++;
                    break;
                }

            case 0:
                if (enemy.getPicture().getX() == player.getXCord() && enemy.getPicture().getY() + 5 == player.getYCord() && !enemy.isDestroyed()) {
                    player.setHealth(enemy.getDmgInflicted());
                    enemy.getPicture().delete();
                    enemy.setDead(true);
                    enemy.getPicture().translate(-1000,50);
                    break;
                } else if (!enemy.isDestroyed()){
                    setEnemy(enemy);
                    enemy.getPicture().translate((-DISTANCE), 0.0);
                    break;
                }  else if (enemy.isDestroyed()) {
                    enemy.setDead(false);
                    enemy.getPicture().delete();
                    enemy.getPicture().translate(-1000,50);
                    player.growXpBar();
                    enemy = createNewEnemy();
                    setEnemy(enemy);
                    enemyCounter++;
                    break;
                }
        }
    }

    public int returnEnemyX() {
        return enemy.getPicture().getX();
    }

    public Enemy returnEnemy() {
        return enemy;
    }

    public Enemy createNewEnemy() {
        return enemy = EnemyFactory.createNewEnemy();
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setEnemy(Enemy enemy) {
        this.enemy = enemy;
    }

    public int getEnemyCounter() {
        return enemyCounter;
    }
}






