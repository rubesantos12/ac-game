package org.academiadecodigo.cunnilinux.enemy;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Knight extends Enemy{

    //Orc constructor
    public Knight(int health, int dmgInflicted) {
        super(health, dmgInflicted, EnemyType.KNIGHT, EnemyType.KNIGHT.getImagePath());
        getPicture().grow(13, 11);
        getPicture().draw();
    }

}
