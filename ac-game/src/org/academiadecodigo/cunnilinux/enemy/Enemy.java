package org.academiadecodigo.cunnilinux.enemy;

import org.academiadecodigo.cunnilinux.Shootable;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Enemy implements Shootable {
    private int health;
    private int dmgInflicted;
    private EnemyType enemyType;
    private EnemyPosition enemyPosition = new EnemyPosition(this);
    private Picture picture;
    private boolean dead;

    public Enemy(int health, int dmgInflicted, EnemyType enemyType, String imagePath) {
        this.health = health;
        this.dmgInflicted = dmgInflicted;
        this.enemyType = enemyType;
        enemyPosition.setEnemy(this);
        picture = new Picture(1000,750,imagePath);
        picture.draw();
        this.dead = false;
    }

    @Override
    public boolean isDestroyed() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

    @Override
    public void hit(int damage) {
        health = health - damage;
    }

    public int getDmgInflicted() {
        return dmgInflicted;
    }

    public EnemyPosition getEnemyPosition() {
        return enemyPosition;
    }

    public void setNewPosition() {
            enemyPosition.enemyNewPosition();
    }

    public Picture getPicture() {
        return picture;
    }

}




