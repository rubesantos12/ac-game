# Risk of Gorilla

Risk of Gorilla is a platform shooter, and it's main goal is to progress through various levels, defeating multiple enemies and leveling up.
The final goal of the game is to defeat the Big Gorilla!

# Technologies
    - Java 7
    - IntelliJ IDEA

# How to play

First of all, you need to clone this repository

```
git clone https://gitlab.com/rubesantos12/ac-game.git
```

and then create a JAR file to then be able to play the game

# Controls

<p align="center">
    <img src="mete aqui os controlos">
</p>

# Screenshots

<p align="center">
    <img src="mete aqui os prints">
</p>